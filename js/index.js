
var quotes=[
  "The way you think is the evaluation of your life progression. -Ivaylo Tonov", 
  "You look hot, why don`t you buy me a beer?  -Ivaylo Tonov",
  "If I lose, I will only lose myself, but if I win, I will win for everybody. -Vasil Levksi",
  "The Freedom is in the sausage... -Sancho Salam",
  "Do not listen to me, watch me what I do... -Bulgarian adage"
];
function getQuote(){
  var randomQuote= quotes[Math.floor(Math.random()*quotes.length)];
  $('#displayQuote').html(randomQuote.replace('-', '<br />' + '— '));  
};
